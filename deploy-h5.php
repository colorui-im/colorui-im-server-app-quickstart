<?php
namespace Deployer;

require 'recipe/common.php';
if(file_exists('./config/common-config-self.php')){
    $configCommon = require_once './config/common-config-self.php';
}else{
    $configCommon = require_once './config/common-config.php';
}

$config = $configCommon['h5'];

/** 必填项 */
set('domain',$config['domain']);//换成自己的域名
// Project name
set('application', $config['application']); //会部署在 /var/www/sites/laravel.deployer.user.jc91715.top下尽量domain一致
// Project repository
set('repository', $config['repository']);//换成自己的
if($config['branch']){
    set('branch', $config['branch']);
}
set('server_ip',$configCommon['server_ip']);//换成自己的
set('server_user',$configCommon['server_user']);//换成自己的
set('identityFile',$configCommon['identityFile']);//换成自己的

set('use_local_release',$config['use_local_release']);
set('local_release_path', $config['local_release_path']);//打包后对代码

set('dnmp_path', $configCommon['dnmp_path']);

/** 必填项 */



// Hosts

host(get('server_ip'))
    ->set('deploy_path', '/var/www/sites/{{application}}')->user(get('server_user'))->identityFile(get('identityFile'));  
set('docker_release_path', function(){
    $releasePath = get('release_path');
    return rtrim(str_replace('/var/www/', '/www/', $releasePath),'/');
});





task('docker_path', function () {
    writeln(get('release_path'));
    writeln('{{deploy_path}}/shared');
    writeln(get('dnmp_path'));
    writeln(get('docker_release_path'));

});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.




//nginx 配置文件
task('nginx:conf',function()use($config){
    $tpl = file_get_contents($config['domain_conf_tpl']);
    $tpl = str_replace('{{application}}', get('application'), $tpl);
    $tpl = str_replace('{{domain}}', get('domain'), $tpl);
    
    if(test("[ -d {{dnmp_path}}/services/nginx/certbot/live/{{domain}} ]")){//是否支持证书
        $tpl = str_replace('{{ssl}}', 'listen 443 ssl;
        ssl_certificate /etc/letsencrypt/live/'.get('domain').'/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/'.get('domain').'/privkey.pem;
        ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers         HIGH:!aNULL:!MD5;', $tpl);
    }else{
        $tpl = str_replace('{{ssl}}', '', $tpl);
    }
    file_put_contents('./domain.conf', $tpl);
    upload('./domain.conf','{{dnmp_path}}/services/nginx/conf.d/'.get('domain').'.conf');
});

task('certbot',function(){//第一次运行单独
    try{
        if (!test("[ -d {{dnmp_path}}/services/nginx/certbot/live/{{domain}} ]")) {
            run('docker exec nginx sh -c  \'rm -rf /var/lib/letsencrypt/temp_checkpoint && certbot --agree-tos -d {{domain}} --nginx --register-unsafely-without-email\'');
        }else{
            writeln('已经申请过证书了');
        }
    } catch(\Exception $e){
        writeln($e->getMessage());
    }
    
}); 



task('nginx:reload',function(){
    run('docker exec nginx sh -c \'if nginx -t 2>/dev/null;then nginx -s reload; else echo "domain.conf 配置有误"; fi\'');
});


task('deploy:update_code', function(){
    $releasePath = parse("{{deploy_path}}/releases/{{release_name}}");
    writeln($releasePath);
    upload('{{local_release_path}}', $releasePath);
});
task('deploy',[
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:symlink',//链接到current
    'deploy:unlock',
    'cleanup',
    'nginx',
    'certbot',
    'nginx',
]);

after('deploy', 'success');

task('nginx',[
    'nginx:conf',
    'nginx:reload'
]);




task('remove:web', function(){
    $deploy_path = get('deploy_path');
    if(test("[ -d {{deploy_path}} ]")){
        if(askConfirmation('请谨慎操作,确定清除网站'.$deploy_path.'?')){
            run('rm -rf {{deploy_path}}');
            writeln('清除网站成功');
        }
    }else{
        writeln('已经清除过网站');
    }

});
task('remove:conf', function(){
    $confPath = parse('{{dnmp_path}}/services/nginx/conf.d/{{domain}}.conf');
    if(test("[ -f {$confPath} ]")){
        if(askConfirmation('请谨慎操作,确定清除nginx配置文件:'.$confPath.'?')){
            run('rm '.$confPath);
            writeln('清除nginx配置文件成功');
        }
    }else{
        writeln('已清除过nginx配置文件');
    }
});

task('clear:web', [
    'remove:web',
    'remove:conf',
]);






