
# 10分钟内一键部署环境及应用?

> 维护了一些项目，每次都要ssh 登录服务器，安装环境，部署项目，在申请证书，支持https,一连串下来，好累。

[dnmp](https://github.com/yeszao/dnmp.git) 配合 [部署工具 deployer](https://deployer.org/)，可以在10分钟内部署一套完整项目，解放双手，可以有更多的时间升级打怪了


 运行环境
* [dnmp](https://github.com/yeszao/dnmp.git) 
* [部署工具 deployer](https://deployer.org/)

## 快速开始

[下载压缩包](https://jc91715.top/storage/app/media/%E9%83%A8%E7%BD%B2/server.zip) 或者 [gitee](https://gitee.com/wpjscc/server-app-quickstart)

### 要求

* 服务器 可以ssh免密码登录
* 用到了80,3306端口，有应用占用的可以停掉或者在docker.env中换一个端口,
* config/common-config.php 里面有一些公共的配置 ,可以修改成自己的,会优先使用config/common-config-self.php


## 一 安装基础环境 
基础环境安装一次即可

```
php vendor/bin/dep -f server.php environment:install -vvv
```

## 二 发布一个laravel

### 1 发布

```
php vendor/bin/dep -f deploy-laravel.php deploy -vvv
```

### 2 清除网站

```
php vendor/bin/dep -f deploy-laravel.php clear:web -vvv
```


### 3 证书定时任务开启
进到服务器

```
crontab -e
0 0 * 1/1 * docker exec nginx certbot renew
```

###  说明
* server.php 是 服务其环境安装
* deploy-*.php 是 应用发布配置，可以自己定义
* docker.env和docker-compose.yml 是安装环境的基础配置。
* ./config/laravel/domain.conf.tpl 是nginx 配置模版文件，可以去掉模版参数。.env.example 会上传到服务器更目录 ,会优先使用.env


### 其他
* services/nginx 下增加了一个certbot用于存放certbot增加的证书，不使用certbot的证书话，可以将自有证书放在ssl下
* services/nginx 下的Dockerfile 增加了certbot-nginx 其他和[dnmp](https://github.com/yeszao/dnmp.git)一样
* 为了在我的nas中运行（配置很低），默认带了 vendor。 可删除掉 运行 composer install


### 问题
* docker启动不起来 可以 docker logs containerId 查看容器日志
* 应用启动不起来，可在 ～/dnmp/logs 下查看相关日志文件
* 应用启动起来了，可查看应用的日志，定位相关问题
* 更多请参考 [dnmp](https://github.com/yeszao/dnmp.git)


## 三 发布一个H5

### 1 发布

```
php vendor/bin/dep -f deploy-h5.php deploy  -vvv 
```

### 2 清除网站

```
php vendor/bin/dep -f deploy-h5.php clear:web -vvv
```

## 四 发布一个 wintercms


### 1 发布
```
php vendor/bin/dep -f deploy-wintercms.php deploy  -vvv 
```

### 2 清除网站

```
php vendor/bin/dep -f deploy-wintercms.php clear:web -vvv
```
 ./config/wintercms/domain.conf.tpl 是nginx 配置模版文件，可以去掉模版参数。.env.example 会上传到服务器更目录 ,会优先使用.env


## 五 单独发布一个 workerman

config/common-config.php 中的 support_workerman_center false 有效

### 1 发布

```
php vendor/bin/dep -f deploy-workermman.php deploy  -vvv 
```


### 2 清除网站

```
php vendor/bin/dep -f deploy-workermman.php clear:web -vvv
```
 ./config/workermman/domain.conf.tpl 是nginx 配置模版文件，可以去掉模版参数。.config.php.tpl 会上传到服务器,会覆盖掉 https://gitee.com/colorui-im/workerman.git 下的 config.php


## 6 发布一个 workerman-center 相当于分布式部署负载均衡

config/common-config.php 中的 workerman-center ，

```
  'workerman_servers' => [//子节点
            'server php:8284;',//自带的子节点
            'server php:8285;',
        ]
```

### 1 发布

```
php vendor/bin/dep -f deploy-workermman-center.php deploy  -vvv 
```


### 2 清除网站

```
php vendor/bin/dep -f deploy-workermman-center.php clear:web -vvv
```
 ./config/workermman/domain.conf.tpl 是nginx 配置模版文件，可以去掉模版参数。.config.php.tpl 会上传到服务器,会覆盖掉 https://gitee.com/colorui-im/workerman.git 下的 config.php

#### 6-1 发布一个workerman子节点
config/common-config.php 中的 support_workerman_center 设为 true

##### 1 发布

```
php vendor/bin/dep -f deploy-workermman.php deploy  -vvv 
```
##### 2 清除子节点

```
php vendor/bin/dep -f deploy-workermman.php clear:web -vvv
```

### 6-2 连接workerman
```
  new websocker('wss://domain.com)
```

### bug

* 1 发布laravel，current 是链接的 4 但api访问一直连接的是1?,带排查。已解决，通过nginx指向发布文件夹


