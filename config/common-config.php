<?php

return [
    'server_ip' => '47.96.15.116',//
    'server_user' => 'root',//root
    'identityFile' => '~/.ssh/id_rsa',//~/.ssh/id_rsa
    'dnmp_path' =>'~/dnmp',//发布到服务器的dnmp的路径

    'laravel' => [
        'domain' => 'colorui-im-admin.test.jc91715.top',//尽量和application保持一致
        'application' => 'colorui-im-admin.test.jc91715.top',//尽量和application保持一致
        'repository' => 'https://gitee.com/colorui-im/colorui-im-admin.git',//代码路径
        'branch' => '',//为空则使用master

        'use_local_release' => false,//使用本地打包后的代码，不使用git代码
        'local_release_path' => '',//末尾带着/ use_local_release 为true 有效
        'domain_conf_tpl' =>'./config/laravel/domain.conf.tpl',
        'composer_options' => '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest',
    ],
    'h5' => [//目前只支持本地代码
        'domain' => 'colorui-im-h5.test.jc91715.top',//尽量和application保持一致
        'application' => 'colorui-im-h5.test.jc91715.top',//尽量和application保持一致
        'repository' => '',//代码路径
        'branch' => '',//为空则使用master

        'use_local_release' => true,//使用本地打包后的代码，不使用git代码
        'local_release_path' => '/Users/jcc/Downloads/front-colorui/unpackage/dist/build/h5/',//末尾带着/ use_local_release 为true 有效
        'domain_conf_tpl' =>'./config/h5/domain.conf.tpl'
        
    ],
    'wintercms' => [
        'domain' => 'wintercms-v1.test.jc91715.top',//尽量和application保持一致
        'application' => 'wintercms-v1.test.jc91715.top',//尽量和application保持一致
        'repository' => 'https://gitee.com/wintercms/winter.git',//代码路径
        'branch' => 'v1.1.2',//为空则使用master

        'use_local_release' => true,//使用本地打包后的代码，不使用git代码
        'local_release_path' => '/Users/jcc/Code/wintercms/',//末尾带着/ use_local_release 为true 有效
        'domain_conf_tpl' =>'./config/wintercms/domain.conf.tpl',
        'composer_options' => '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --no-suggest',
        
    ],
    'workerman-center' => [//可在一个docker外启动，这里放在docker中
        'services' => [
            'register',
            'gateway',
            'businessworker'
        ],//可把gateway，businessworker去掉，只起注册中心，这里加上去，默认有一个服务提供，可靠性
        'domain' => 'colorui-im-workerman-center.test.jc91715.top',//尽量和application保持一致，这里的域名要和子节点的域名保持一致，会生成一个nginx配置文件
        'application' => 'colorui-im-workerman-center.test.jc91715.top',//尽量和application保持一致，这里可以不保持一致
        'repository' => 'https://gitee.com/colorui-im/workerman.git',//代码路径
        'branch' => '',//为空则使用master

        'use_local_release' => false,//使用本地打包后的代码，不使用git代码
        'local_release_path' => '/Users/jcc/Code/workerman-center/',//末尾带着/ use_local_release 为true 有效
        'domain_conf_tpl' =>'./config/workerman-center/domain.conf.tpl',

        'registerPort' => 1240,//在start_register.php 中

        //下方是和注册中心一起的，也可以去掉
        'gatewayWebsockertPort' => 8284, //在 start_gateway.php 中
        'gatewayStartPort' => 5000, //在 start_gateway.php 中
        'gatewaylanIp' => '127.0.0.1', //在 start_gateway.php 中 本机ip，分布式部署时使用内网ip
        'registerAddress' => '127.0.0.1:1240',//start_gateway.php start_businessworker.php 中 （注册中心地址）
        'workerman_servers' => [//子节点
            'server php:8284;',
            'server php:8285;',
        ]
    ],
    'workerman' => [
        'support_workerman_center' => true,//是否向workerman-center 注册,

        'services' => [
            // 'register', //support_workerman_center false 时，取消注释
            'gateway',
            'businessworker'
        ],//没有注册中心

        'domain' => 'colorui-im-workerman.test.jc91715.top',//尽量和application保持一致 support_workerman_center 为true 时无效，单独起服务时避免和 workerman_center一样
        'application' => 'colorui-im-workerman.test.jc91715.top',//尽量和application保持一致
        'repository' => 'https://gitee.com/colorui-im/workerman.git',//代码路径
        'branch' => '',//为空则使用master

        'use_local_release' => false,//使用本地打包后的代码，不使用git代码
        'local_release_path' => '/Users/jcc/Code/workerman/',//末尾带着/ use_local_release 为true 有效
        'domain_conf_tpl' =>'./config/workerman/domain.conf.tpl',

        'registerPort' => 1240,//在start_register.php 中
        'gatewayWebsockertPort' => 8285, //在 start_gateway.php 中
        'gatewayStartPort' => 5010, //在 start_gateway.php 中
        'gatewaylanIp' => '127.0.0.1', //在 start_gateway.php 中 本机ip，分布式部署时使用内网ip
        'registerAddress' => '127.0.0.1:1240',//start_gateway.php start_businessworker.php 中 （注册中心地址）
        
    ],
];