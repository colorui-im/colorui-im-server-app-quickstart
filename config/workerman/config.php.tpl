<?php
//会覆盖掉 workerman-gateway 根目录下的config.php
return [
    'services'=> {{services}},
    'registerPort'=> {{registerPort}},
    'gatewayWebsockertPort'=> {{gatewayWebsockertPort}},
    'gatewayStartPort'=> {{gatewayStartPort}},
    // 'ssl'=>{{enable_ssl}},
    // 'context' => array(
    //         // 更多ssl选项请参考手册 http://php.net/manual/zh/context.ssl.php
    //         'ssl' => array(
    //             // 请使用绝对路径
    //             'local_cert'                 => '{{cer}}', // 也可以是crt文件
    //             'local_pk'                   => '{{key}}',
    //             'verify_peer'               => false,
    //             // 'allow_self_signed' => true, //如果是自签名证书需要开启此选项
    //         )
    // ),
    'gatewaylanIp' => '{{gatewaylanIp}}',//本机ip，分布式部署时使用内网ip
    'registerAddress' => '{{registerAddress}}',
    
];
