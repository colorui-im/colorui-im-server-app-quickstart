
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}
upstream websocket {
    #ip_hash;
    #server php:{{port}};  
    {{workerman_servers}}
}
 
 server {
    listen *:80;
    server_name {{domain}};


    location / {
      proxy_pass http://websocket;
      proxy_read_timeout 300s;

      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection $connection_upgrade;
    }

    access_log /var/log/nginx/{{domain}}.access.log;
    error_log  /var/log/nginx/{{domain}}.error.log;

    {{ssl}}
}


