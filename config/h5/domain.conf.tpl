server {
    listen *:80;
    server_name {{domain}};

    root /www/sites/{{application}}/current;
    index  index.html index.htm;

    location / {
        try_files $uri $uri/ /index.html;
	    index index.html;
    }

    location ~ /.well-known {
        allow all;
    }

    access_log /var/log/nginx/{{domain}}.access.log;
    error_log  /var/log/nginx/{{domain}}.error.log;
    client_max_body_size 200M;

    {{ssl}}

    if ($scheme != "https") {
        return 301 https://$host$request_uri;
    } # managed by Certbot
}